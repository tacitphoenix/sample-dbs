## Menagerie Database

This directory contains files that can be used to set up the menagerie
database that is used in the tutorial chapter of the MySQL Reference
Manual.

## Installation

```shell
./setup_menagerie_db.sh
```
