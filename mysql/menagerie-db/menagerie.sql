# drop and create menagerie databaes
DROP SCHEMA IF EXISTS menagerie;
CREATE SCHEMA menagerie;
USE menagerie;

# drop the pet table if it exists, then recreate it
DROP TABLE IF EXISTS pet;

CREATE TABLE pet
(
  name    VARCHAR(20),
  owner   VARCHAR(20),
  species VARCHAR(20),
  sex     CHAR(1),
  birth   DATE,
  death   DATE
);

# Allow local file loading
SET GLOBAL local_infile = 'ON';

# empty the table load new records into it
DELETE FROM pet;
INSERT INTO pet VALUES ('Puffball','Diane','hamster','f','1999-03-30',NULL);
LOAD DATA LOCAL INFILE './data/pet.txt' INTO TABLE pet;

# drop the event table if it exists, then recreate it
DROP TABLE IF EXISTS event;

CREATE TABLE event
(
  name   VARCHAR(20),
  date   DATE,
  type   VARCHAR(15),
  remark VARCHAR(255)
);

# empty the table load new records into it
DELETE FROM event;
LOAD DATA LOCAL INFILE './data/event.txt' INTO TABLE event;
